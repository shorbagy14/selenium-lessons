package com.learnitbro.selenium.run;

import java.io.File;
import java.io.InputStream;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.learnitbro.selenium.inputstream.InputStreamController;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Operation {

	private WebDriver driver;
	private static String browser = "chrome";
	private static String website = "https://www.convert-jpg-to-pdf.net/";

	public static void main(String[] args) {

		Operation operation = new Operation();
		operation.setupDriver(browser);
		operation.setupWebsite(website);
		// operation.setupTest();
	}

	void setupDriver(String driverType) {
		System.out.println("Driver type: " + driverType);
		InputStreamController i = new InputStreamController();
		switch (driverType.toLowerCase()) {
		case "chrome":
			// WebDriverManager.chromedriver().setup();
			
			InputStream stream = getClass().getResourceAsStream("/chromedriver");
			File chromedriver = i.convertInputStreamToFile(stream, new File("temp/chromedriver"));
			System.setProperty("webdriver.chrome.driver", chromedriver.getAbsolutePath());
			
//			System.setProperty("webdriver.chrome.driver", "chromedriver");
			driver = new ChromeDriver();
			break;

		case "firefox":
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			break;

		case "ie":
			WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
			break;

		default:
			throw new WebDriverException("Driver type is not defined");
		}
		driver.manage().window().fullscreen();
		// driver.manage().window().maximize();
	}

	void setupWebsite(String websiteLink) {
		driver.get(websiteLink);
		// System.out.println("Source: " + driver.getPageSource());
		System.out.println("Url: " + driver.getCurrentUrl());
		System.out.println("Title: " + driver.getTitle());
	}

	void setupTest() {
		Coordinator coordinator = new Coordinator(driver);
		coordinator.runTests();
	}
}
