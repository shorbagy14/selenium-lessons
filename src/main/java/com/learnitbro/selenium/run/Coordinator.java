package com.learnitbro.selenium.run;

import org.openqa.selenium.WebDriver;

import com.learnitbro.selenium.tests.Test;

public class Coordinator {
	
	private WebDriver driver;
	
	@SuppressWarnings("unused")
	private Coordinator() {
		// Leave Empty -- Always keep this constructor private
		throw new NullPointerException("You forgot to pass the driver to this class: " + this.getClass().getName());
	}
	
	public Coordinator(WebDriver driver) {
		this.driver = driver;
	}
	
	public void runTests() {
		Test test = new Test(driver);
		test.uploadTest();
	}
}
