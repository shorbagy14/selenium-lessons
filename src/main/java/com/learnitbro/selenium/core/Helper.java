package com.learnitbro.selenium.core;

import org.openqa.selenium.WebDriver;

public class Helper {

	// Sanity check for driver
	public static void driverSanity(WebDriver driver) {
		if (driver == null) {
			throw new NullPointerException("Your driver is null");
		} else {
			System.out.println("Driver hash code: " + driver.manage().hashCode());
		}
	}
}
