package com.learnitbro.selenium.lessons;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.learnitbro.selenium.file.FileController;

public class Upload {

	private WebDriver driver;

	@SuppressWarnings("unused")
	private Upload() {
		// Leave Empty -- Always keep this constructor private
		throw new NullPointerException("You forgot to pass the driver to this class: " + this.getClass().getName());
	}

	public Upload(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * Method Name: uploadFile() Use: To upload single/multiple files
	 * 
	 * @param
	 * 
	 * Argument 1: By - Selector of the element 
	 * Example: ("//input[@type='file']")
	 * 
	 * Argument 2: String - Path of the file 
	 * Example: ("/animal.jpg")
	 * 
	 * @return Name of the file(s) being uploaded - String
	 * 
	 **/

	// Allows single file upload
	public String uploadSingleFile(By element, String path) {
		driver.findElement(element).sendKeys(path);
		System.out.println("Uploading : " + path);
		return path;
	}

	// Allows multiple files upload
	public String uploadMultipleFiles(By element, String[] listOfPaths) {
		Collection<String> files = Arrays.asList(listOfPaths);
		String result = files.stream().collect(Collectors.joining("\n"));
		driver.findElement(element).sendKeys(result);
		System.out.println("Uploading : \n" + result);
		return result;
	}

	// Allows single or multiple files upload (Recommended)
	public String uploadFile(By element, String... listOfPaths) {
		System.out.println("Files to upload : " + listOfPaths.length);
		Collection<String> files = Arrays.asList(listOfPaths);
		String result = files.stream().collect(Collectors.joining("\n"));
		driver.findElement(element).sendKeys(result);
		System.out.println("Uploading : \n" + result);
		return result;
	}

	// Example for uploads

	private void uploadTest1() {
		Upload upload = new Upload(driver);
		By input = By.xpath("//input[@type='file']");
		// ** Method 1 **
		String path = "/Users/shorbagy14/Desktop/Education/123.jpg";
		upload.uploadSingleFile(input, path);
	}

	private void uploadTest2() {
		Upload upload = new Upload(driver);
		By input = By.xpath("//input[@type='file']");
		// ** Method 2 **
		String[] listOfPaths = { "/Users/shorbagy14/Desktop/Education/123.jpg",
				"/Users/shorbagy14/Desktop/Education/abc.jpg" };
		upload.uploadMultipleFiles(input, listOfPaths);
	}

	private void uploadTest3() {
		Upload upload = new Upload(driver);
		By input = By.xpath("//input[@type='file']");
		// ** Method 3 **
		File location = new File("/Users/shorbagy14/Desktop/Education/");
		// Refer to the helper class at : https://bitbucket.org/shorbagy14/selenium-lessons/src/master/src/learnitbro/selenium/Helper.java
		FileController f = new FileController();
		File[] files = f.getFiles(location, "jpg");
		String listOfPaths[] = Arrays.stream(files).map(File::getAbsolutePath).toArray(String[]::new);
		upload.uploadFile(input, listOfPaths);
	}
}
