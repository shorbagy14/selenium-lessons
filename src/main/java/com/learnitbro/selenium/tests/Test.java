package com.learnitbro.selenium.tests;

import java.io.File;
import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.learnitbro.selenium.core.Helper;
import com.learnitbro.selenium.file.FileController;
import com.learnitbro.selenium.lessons.Upload;

public class Test {
	
	private WebDriver driver;
	
	@SuppressWarnings("unused")
	private Test() {
		// Leave Empty -- Always keep this constructor private
		throw new NullPointerException("You forgot to pass the driver to this class: " + this.getClass().getName());
	}

	public Test(WebDriver driver) {
		this.driver = driver;
		Helper.driverSanity(this.driver);
	}
	
	public void uploadTest() {
		Upload upload = new Upload(driver);
		By input = By.xpath("//input[@type='file']");
		File location = new File("/Users/shorbagy14/Desktop/Education/");
		FileController f = new FileController();
		File[] files = f.getFiles(location, "jpg");
		String[] listOfPaths = Arrays.stream(files).map(File::getAbsolutePath).toArray(String[]::new);
		upload.uploadFile(input,listOfPaths);
	}
}
