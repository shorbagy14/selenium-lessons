package com.learnitbro.selenium.inputstream;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermissions;

import org.apache.commons.io.FileUtils;

public class InputStreamController {

	public File convertInputStreamToFile(InputStream initialStream, File targetFile) {
		try {
			FileUtils.copyInputStreamToFile(initialStream, targetFile);
			Files.setPosixFilePermissions(targetFile.toPath(), PosixFilePermissions.fromString("rwxr-xr-x"));
			return targetFile;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
