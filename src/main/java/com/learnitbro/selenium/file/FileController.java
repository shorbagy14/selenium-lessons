package com.learnitbro.selenium.file;

import java.io.File;
import java.io.FilenameFilter;

public class FileController {
	
	/**
	 * Method Name: getFiles()
	 * Use: TGet files from a folder that has a specific extension
	 *  
	 * @param
	 * Argument 1: File - Location of the directory
	 * Example: ("folder/")
	 *  
	 * Argument 2: String - File extension
	 * Example: ("jpg")
	 *  
	 * @return
	 * Name of the files within that directory - File []
	 *  
	 **/ 

	// Get files from a folder that has a specific extension
	public File[] getFiles(File location, String extension) {
		File[] files = location.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(extension);
			}
		});
		return files;
	}

}
